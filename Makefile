DESTDIR =
PREFIX = /usr/local
BINDIR = /bin
DATADIR = /share/cross-install
MANDIR = /share/man

all: lib/getoptions.sh

lib/getoptions.sh: src/getoptions.sh
	gengetoptions parser -f src/getoptions.sh --shellcheck parser_definition getoptions_parse > lib/getoptions.sh

clean:
	rm lib/getoptions.sh

check: bin/cross-install
	shellcheck -x bin/cross-install

install: bin/cross-install lib/getoptions.sh man/cross-install.1.man
	bin/cross-install -dm 755 -t $(DESTDIR)$(PREFIX)$(BINDIR) \
		bin/cross-install
	sed -e 's#^DEBUG=.*#DEBUG='\''false'\''#' \
		-e 's#^DATADIR=.*#DATADIR='\''$(PREFIX)$(DATADIR)'\''#' \
		-e 's#^CONFDIR=.*#CONFDIR='\''$(CONFDIR)'\''#' \
		$(DESTDIR)$(PREFIX)$(BINDIR)/cross-install \
		| sponge $(DESTDIR)$(PREFIX)$(BINDIR)/cross-install
	bin/cross-install -dm 644 -t $(DESTDIR)$(PREFIX)$(DATADIR) \
		lib/getoptions.sh
	bin/cross-install -dm 644 man/cross-install.1.man \
		$(DESTDIR)$(PREFIX)$(MANDIR)/man1/cross-install.1

uninstall:
	rm -f $(DESTDIR)$(PREFIX)$(BINDIR)/cross-install
	rm -rf $(DESTDIR)$(PREFIX)$(DATADIR)
	rm -f $(DESTDIR)$(PREFIX)$(MANDIR)/man1/cross-install.1

.PHONY: all clean check install uninstall
