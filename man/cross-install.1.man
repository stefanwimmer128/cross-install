.TH "cross-install" "1" 

.SH "NAME"
.PP
cross-install - POSIX-complient cross-platform install command

.SH "DESCRIPTION"
.PP
POSIX-complient shell script to use the install-utility cross-platform with unified options and without dependencies.

.SH "USAGE"
.SS "cross-install [OPTIONS]... [ARGUMENTS]..."
.PP
Call cross-install with options and arguments.

.SS "cross-install <SOURCE> <TARGET>"
.PP
Install SOURCE to TARGET.

.SS "cross-install -t <TARGET-DIRECTORY> <SOURCE>..."
.PP
Install SOURCE into TARGET-DIRECTORY.

.SH "ARGUMENTS"
.SS "SOURCE"
.PP
Source file or directory when using -r/--recursive. When using -t/--target-directory multiple files (or directories with -r/--recursive) can be provided.

.SS "TARGET"
.PP
Target file or directory when using -r/--recursive. Unused when using -t/--target-directory.

.SH "OPTIONS"
.SS "-b, --backup[=BACKUP]"
.PP
Backup file by renaming the original file by appending BACKUP. BACKUP defaults to “.bak”.

.SS "-r, --recursive"
.PP
Installs directories recursively.

.SS "-d, --directory"
.PP
Creates required parent target directories.

.SS "-m, --mode MODE"
.PP
Sets file permissions. See chmod(1).

.SS "-o, --owner OWNER"
.PP
Sets file owner. See chown(1).

.SS "-g, --group GROUP"
.PP
Sets file group. See chown(1).

.SS "-t, --target-directory TARGET-DIRECTORY"
.PP
Installs all files from SOURCE to TARGET-DIRECTORY.

.SS "-v, --verbose"
.PP
Verbose output for each copy operation.

.SS "-h, --help"
.PP
Show help message.

.SS "-V, --version"
.PP
Show program version.

.SH "AUTHORS"
.PP
Stefan Wimmer <info@stefanwimmer128.xyz>
