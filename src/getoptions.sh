# shellcheck shell=sh

parser_definition() {
    setup REST help:getoptions_help -- 'Usage: cross-install [OPTIONS]... <ARGUMENTS>...'
    msg --
    msg -- 'Usage: cross-install <SOURCE> <TARGET>'
    msg -- 'Usage: cross-install -t <TARGET-DIRECTORY> <SOURCE>...'
    msg --
    msg -- 'ARGUMENTS:'
    msg label:SOURCE -- 'Source file/directory, can be muliple when using -t/--target-directory'
    msg label:TARGET -- 'Target file/directory, not used when using -t/--target-directory'
    msg --
    msg -- 'OPTIONS:'
    option backup -b --backup var:BACKUP on:.bak -- 'Backup file by renaming the original file by appending BACKUP [default: .bak]'
    flag recursive -r --recursive -- 'Install directories recursively'
    flag directory -d --directory -- 'Create required parent target directories'
    param mode -m --mode var:MODE -- 'Set permission (see chmod)'
    param owner -o --owner var:OWNER -- 'Set owner (see chown)'
    param group -g --group var:GROUP -- 'Set group (see chown)'
    param target_directory -t --target-directory var:TARGET-DIRECTORY -- 'Install all files to target directory'
    flag verbose -v --verbose -- 'Output for copy operations'
    disp :getoptions_help -h --help -- 'Show help'
    disp VERSION -V --version -- 'Show version'
}
